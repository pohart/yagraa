package com.pohart.goodreads;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class Authenticate extends Activity{
	public static final String REQUEST_TOKEN_SECRET ="com.pohart.goodreads.REQUEST_TOKEN_SECRET";
	public static final String REQUEST_TOKEN = "com.pohart.goodreads.REQUEST_TOKEN";
	public static final String CONSUMER_SECRET = "com.pohart.goodreads.CONSUMER_SECRET";
	public static final String CONSUMER_KEY = "com.pohart.goodreads.CONSUMER_KEY";
	public static final String CALLBACK_URL = "com.pohart.goodreads.CALLBACK_URL";

	public static final OAuthProvider provider = new DefaultOAuthProvider(
					"http://www.goodreads.com/oauth/request_token",
					"http://www.goodreads.com/oauth/access_token",
					"http://www.goodreads.com/oauth/authorize");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		if(extras == null)
		{
			return;
		}
		String accessToken = extras.getString(CONSUMER_KEY);
		String accessSecret = extras.getString(CONSUMER_SECRET);
		String callbackUrl = extras.getString(CALLBACK_URL);

		final OAuthConsumer consumer = new DefaultOAuthConsumer(
						accessToken,
						accessSecret);

		/****************************************************
		 * The following steps should only be performed ONCE
		 ***************************************************/
		try {
			String authUrl = provider.retrieveRequestToken(consumer,getIntent().getDataString());

			SharedPreferences sharedPreferences = getPreferences(this);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(REQUEST_TOKEN, consumer.getToken());
			editor.putString(REQUEST_TOKEN_SECRET, consumer.getTokenSecret());
			editor.putString(CONSUMER_KEY, accessToken);
			editor.putString(CONSUMER_SECRET, accessSecret);
			editor.putString(CALLBACK_URL, callbackUrl);
			editor.commit();

			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)));
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	/** Called when the activity is resumed. */
	@Override
	public void onResume()
	{
		super.onResume();
		// We might be resuming due to the web browser sending us our
		// access tokens.  If so, save them and finish.
		Uri uri = this.getIntent().getData();
		String consumerKey = getPreferences(this).getString(CONSUMER_KEY,null);
		String consumerSecret = getPreferences(this).getString(CONSUMER_SECRET,null);
		String callbackUrl = getPreferences(this).getString(CALLBACK_URL,null);
		String requestToken = getPreferences(this).getString(REQUEST_TOKEN,null);
		String requestTokenSecret = getPreferences(this).getString(REQUEST_TOKEN_SECRET, null);

		final OAuthConsumer consumer = new DefaultOAuthConsumer(
						consumerKey,
						consumerSecret);
		if (uri != null && uri.toString().startsWith(getIntent().getDataString())) {
			String oauthToken = uri.getQueryParameter(OAuth.OAUTH_TOKEN);
			if(oauthToken == null)
			{
				return;
			}
			try {
				// this will populate token and token_secret in consumer
				// Crazy sauce can happen here. Believe it or not, the entire app may have been flushed
				// from memory while the browser was active.
				if (requestToken.length() == 0 || requestTokenSecret.length() == 0) {
					Toast.makeText(this, "The request tokens were lost, please close the browser and application and try again.", Toast.LENGTH_LONG).show();
					finish();
					return;
				}
				consumer.setTokenWithSecret(requestToken, requestTokenSecret);
				provider.retrieveAccessToken(consumer, oauthToken);

				String userId = "";

				String token = consumer.getToken();
				String tokenSecret = consumer.getTokenSecret();
				URLConnection getAuthUserRequest = new URL("http://www.goodreads.com/api/auth_user").openConnection();
				consumer.sign(getAuthUserRequest);
				consumer.setTokenWithSecret(token, tokenSecret);
				SharedPreferences.Editor editor = getPreferences(this).edit();
				editor.putString(REQUEST_TOKEN, token);
				editor.putString(REQUEST_TOKEN_SECRET, tokenSecret);
				editor.commit();

				Intent callback = new Intent(Intent.ACTION_VIEW,Uri.parse(callbackUrl));
				callback.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(callback);
			} catch (OAuthMessageSignerException e1) {
				Toast.makeText(this, "Message signer exception:\n" + e1.getMessage(), Toast.LENGTH_LONG).show();
				finish();
				return;
			} catch (OAuthNotAuthorizedException e1) {
				Toast.makeText(this, "Not Authorized Exception:\n" + e1.getMessage(), Toast.LENGTH_LONG).show();
				finish();
				return;
			} catch (OAuthExpectationFailedException e1) {
				Toast.makeText(this, "Expectation Failed Exception:\n" + e1.getMessage(), Toast.LENGTH_LONG).show();
				finish();
				return;
			} catch (OAuthCommunicationException e1) {
				Toast.makeText(this, "Communication Exception:\n" + e1.getMessage(), Toast.LENGTH_LONG).show();
				finish();
				return;
			} catch (IOException e) {
				Toast.makeText(this, "Communication Exception:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
				finish();
				return;
			}
		}
	}
	private String readStream(InputStream inputStream) {
		java.util.Scanner s = new java.util.Scanner(inputStream).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public static SharedPreferences getPreferences(Context ctx) {
		return ctx.getSharedPreferences(ctx.getPackageName() + ".goodreads", MODE_PRIVATE);
	}
	public static boolean isAuthenticated(Context ctx) {
		SharedPreferences preferences = getPreferences(ctx);
		return preferences.contains(REQUEST_TOKEN)
						&& preferences.contains(REQUEST_TOKEN_SECRET)
						&& preferences.contains(CONSUMER_KEY)
						&& preferences.contains(CONSUMER_SECRET)
						&& preferences.contains(CALLBACK_URL);
	}
}
