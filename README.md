Yet Another Goodreads Android Api

library to ease interaction with [goodreads](http:www.goodreads.com), based off [Jade Mason's](http://onesadjam.blogspot.com/) [yagrac](https://code.google.com/p/yagrac/)

How to Use
---
* Get a [Developer Key](https://www.goodreads.com/api/keys) from goodreads.

* In your manifest set com.pohart.goodreads.Authenticate to handle a URI:

		<activity
			android:name="com.pohart.goodreads.Authenticate" android:enabled="true" >
			<intent-filter>
				<action android:name="android.intent.action.VIEW" />
				<category android:name="android.intent.category.DEFAULT" />
				<category android:name="android.intent.category.BROWSABLE" />
				<data android:scheme="yourschema" android:host="goodreads-authenticate"/>
			</intent-filter>
		</activity>

* In your manifest set the activity that launches after authentication to handle a URI:

		<activity 
		    android:name=".GoodreadsActivity"
		    android:enabled="true">
			<intent-filter>
				<action android:name="android.intent.action.VIEW"/> 
				<category android:name="android.intent.category.DEFAULT"/>
				<category android:name="android.intent.category.BROWSABLE"/>
				<data android:scheme="yourschema" android:host="goodreads"/>
			</intent-filter>
		</activity>

* Launch the authentication activity:

		ResponseParser parser = new ResponseParser(this)
		if( ! parser.isAuthenticated()) {
			Intent goodReadsIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("yourschema://goodreads-authenticate"));
			goodReadsIntent.putExtra(Authenticate.CONSUMER_KEY, ACCESS_TOKEN);
			goodReadsIntent.putExtra(Authenticate.CALLBACK_URL, "yourschema://goodreads");
			goodReadsIntent.putExtra(Authenticate.CONSUMER_SECRET, TOKEN_SECRET);
			startActivity(goodReadsIntent);
		} else {
			//you are authenticated
			List<Review> books = parser.getBookOnShelf("to-read",1);
			...
		}

Thats It!

After you have authenticated you can access all api functionality through ResponseParser.

Known Issues
---
1. When Calling back to the application, Authenticate does not remove it's activities from the stack.
1. Only getBooksOnShelf finds the user based on the currently logged in user, all other user based calls require the useid.  The information is trivially there for all of them.
1. Inconsistant naming: Almost all methods in ResponseParser are named using an uppercase first character.
